﻿# **Podatek** #
* * *
## von: Team Avocado ##
 
### Mitglieder: Martin Ehmke, Martin Johnki, Marcus Kopp ###
 
Die App Podatek dient dazu, der Nutzerin eine einfache Möglichkeit zu bieten, ihre Steuererklärung termingerecht abzugeben.
Es ist möglich, Erinnerungen zu erstellen, welche auch mit der Standard-Kalender-App synchronisiert werden können.
Die App bietet die Möglichkeit, sich über die Postleitzahl das eigene Finanzamt rauszusuchen, zu speichen, und sich relevante Daten wie Öffnungszeiten, Telefonnummern, oder genauen Standort anzeigen zu lassen.


### Vorausetzungen:
* mindestens Android Version 4.2 (Jelly Bean)
* Internetzugriff
* Kalenderzugriff (optional)
* Zugriff auf internen Speicher
