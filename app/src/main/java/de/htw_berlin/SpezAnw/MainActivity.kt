package de.htw_berlin.SpezAnw

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import de.htw_berlin.SpezAnw.fragment.HomeFragment
import de.htw_berlin.SpezAnw.manager.PermissionManager
import de.htw_berlin.SpezAnw.notification.NotificationScheduler
import java.util.*


class MainActivity : AppCompatActivity(){

    private var hour: Int = 11
    private var min: Int = 24

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, min)
        calendar.set(Calendar.SECOND, 0)

        NotificationScheduler.setReminder(this@MainActivity, calendar)

        PermissionManager.requestPermission(this)

        supportFragmentManager.beginTransaction().replace(R.id.root_layout, HomeFragment.newInstance()).commit()
    }

    override fun onBackPressed() {
        if (fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
