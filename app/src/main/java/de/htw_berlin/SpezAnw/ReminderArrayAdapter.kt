package de.htw_berlin.SpezAnw

import android.content.Context
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton


class ReminderArrayAdapter(context: Context, private val values: List<String>, val handler : ReminderArrayAdapterHandler) : ArrayAdapter<String>(context, -1, values) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.list_reminder, parent, false)
        val textView = rowView.findViewById(R.id.label) as TextView
        val update = rowView.findViewById(R.id.update) as ImageButton
        val delete = rowView.findViewById(R.id.delete) as ImageButton
        delete.setOnClickListener( { v ->  handler.onDeleteReminder(position)})
        update.setOnClickListener({ v ->  handler.onUpdateReminder(position)})
        textView.text = values[position]
        return rowView
    }

    interface ReminderArrayAdapterHandler{
        fun onDeleteReminder(position: Int)
        fun onUpdateReminder(position: Int)
    }
}