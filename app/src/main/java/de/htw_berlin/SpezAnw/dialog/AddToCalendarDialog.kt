package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import de.htw_berlin.SpezAnw.R
import java.text.SimpleDateFormat
import java.util.*

class AddToCalendarDialog(context: Context, date : Calendar, listenerOk : DialogInterface.OnClickListener, listenerCancel : DialogInterface.OnClickListener) : AlertDialog.Builder(context) {

    init {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)

        setTitle(R.string.reminder_management_fragment_addToCalendarDialog_headline)
        setMessage(context.getString(R.string.reminder_management_fragment_addToCalendarDialog_text, dateFormat.format(date.time)))
        setPositiveButton(R.string.reminder_management_fragment_addToCalendarDialog_add, listenerOk)
        setNegativeButton(R.string.reminder_management_fragment_addToCalendarDialog_cancel, listenerCancel )
    }
}