package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import de.htw_berlin.SpezAnw.R

class ErrorCalendarDialog(context: Context) : AlertDialog.Builder(context){

    init {
        setTitle(R.string.reminder_management_fragment_errorCalendar_title)
        setMessage(R.string.reminder_management_fragment_errorCalendar_text)
        setPositiveButton(R.string.reminder_management_fragment_errorCalendar_ok, null )
    }
}