package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import de.htw_berlin.SpezAnw.R

class FinanceOfficeFoundDialog(context: Context, name : String, listener : DialogInterface.OnClickListener) : AlertDialog.Builder(context) {

    init {
        setTitle(R.string.SetAddressFragment_alertSuccess_headline)
        setMessage(context.getString(R.string.SetAddressFragment_alertSuccess_text, name))
        setPositiveButton(R.string.SetAddressFragment_save, listener)
        setNegativeButton(R.string.SetAddressFragment_alert_backText, null )
    }

}