package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import de.htw_berlin.SpezAnw.R

class InvalidDateDialog(context: Context) : AlertDialog.Builder(context){

    init {
        setTitle(R.string.reminder_management_fragment_invalidDateDialog_title)
        setMessage(R.string.reminder_management_fragment_invalidDateDialog_text)
        setPositiveButton(R.string.reminder_management_fragment_invalidDateDialog_ok, null )
    }
}