package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import de.htw_berlin.SpezAnw.R

class NoInternetDialog(context: Context) : AlertDialog.Builder(context){

    init {
        setTitle(R.string.SetAddressFragment_alertInternet_headline)
        setMessage(R.string.SetAddressFragment_alertInternet_text)
        setPositiveButton(R.string.SetAddressFragment_alert_backText, null )
    }
}