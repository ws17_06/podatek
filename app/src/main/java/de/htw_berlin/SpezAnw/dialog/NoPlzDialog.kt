package de.htw_berlin.SpezAnw.dialog

import android.app.AlertDialog
import android.content.Context
import de.htw_berlin.SpezAnw.R

class NoPlzDialog(context: Context, plz : String) : AlertDialog.Builder(context){

    init{
        setTitle(R.string.SetAddressFragment_alertPLZ_headline)
        setMessage(context.getString(R.string.SetAddressFragment_alertPLZ_text, plz))
        setPositiveButton(R.string.SetAddressFragment_alert_backText, null )
    }

}