package de.htw_berlin.SpezAnw.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.htw_berlin.SpezAnw.R
import de.htw_berlin.SpezAnw.databinding.FragmentHomeBinding
import de.htw_berlin.SpezAnw.manager.FinanceManager
import de.htw_berlin.SpezAnw.manager.ReminderManager


class HomeFragment : Handler, Fragment() {

    private val financeManager = FinanceManager()
    private val reminderManager : ReminderManager
        get() = ReminderManager(context)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?) : View? {
        val binding = FragmentHomeBinding.inflate(layoutInflater)
        binding.handler = this
        binding.financeOfficeName = financeManager.load(context)?.name
        binding.reminders = reminderManager.load().reminders.size

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onSetFinanceOfficeClick(){
        activity.supportFragmentManager.beginTransaction().replace(R.id.root_layout, SetAddressFragment.newInstance()).addToBackStack(SetAddressFragment.javaClass.simpleName).commit()
    }

    override fun onShowFinanceOfficeClick(){
        activity.supportFragmentManager.beginTransaction().replace(R.id.root_layout, InfoFragment.newInstance()).addToBackStack(InfoFragment.javaClass.simpleName).commit()
    }

    override fun onSetReminderClick(){
        activity.supportFragmentManager.beginTransaction().replace(R.id.root_layout, ReminderManagementFragment.newInstance()).addToBackStack(ReminderManagementFragment.javaClass.simpleName).commit()
    }

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }
}

interface Handler {
    fun onSetFinanceOfficeClick()
    fun onShowFinanceOfficeClick()
    fun onSetReminderClick()
}
