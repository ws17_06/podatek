package de.htw_berlin.SpezAnw.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.htw_berlin.SpezAnw.R
import de.htw_berlin.SpezAnw.databinding.FragmentInfoBinding
import de.htw_berlin.SpezAnw.manager.FinanceManager
import kotlinx.android.synthetic.main.fragment_info.*

class InfoFragment : Fragment(), InfoFragmentHandler {

    private val financeManager = FinanceManager()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val binding = FragmentInfoBinding.inflate(inflater)
        binding.handler = this
        binding.financeOffice = financeManager.load(context)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        office_name.text = getString(R.string.info_fragment_finance_office, financeManager.load(context)?.name)
    }

    override fun onBack(){
        activity.supportFragmentManager.beginTransaction().replace(R.id.root_layout, HomeFragment.newInstance()).addToBackStack(HomeFragment.javaClass.simpleName).commit()
    }

    override fun onChange() {
        activity.supportFragmentManager.beginTransaction().replace(R.id.root_layout, SetAddressFragment.newInstance()).addToBackStack(SetAddressFragment.javaClass.simpleName).commit()
    }

    override fun onShowOnMap() {
        val mapIntent = Intent(Intent.ACTION_VIEW)
        mapIntent.data = Uri.parse("geo:0,0?q=${financeManager.load(context)?.strasse}")
        startActivity(mapIntent)
    }

    companion object {
        fun newInstance(): InfoFragment {
            return InfoFragment()
        }
    }

}

interface InfoFragmentHandler {
    fun onBack()
    fun onChange()
    fun onShowOnMap()
}
