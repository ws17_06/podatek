package de.htw_berlin.SpezAnw.fragment

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.htw_berlin.SpezAnw.R
import de.htw_berlin.SpezAnw.ReminderArrayAdapter
import de.htw_berlin.SpezAnw.databinding.FragmentReminderManagementBinding
import de.htw_berlin.SpezAnw.dialog.AddToCalendarDialog
import de.htw_berlin.SpezAnw.dialog.InvalidDateDialog
import de.htw_berlin.SpezAnw.manager.ReminderManager
import de.htw_berlin.SpezAnw.model.Reminder
import de.htw_berlin.SpezAnw.notification.NotificationScheduler
import kotlinx.android.synthetic.main.fragment_reminder_management.*
import java.text.SimpleDateFormat
import java.util.*

class ReminderManagementFragment : Fragment(), ReminderManagementFragmentHandler, ReminderArrayAdapter.ReminderArrayAdapterHandler {

    private val reminderString = ArrayList<String>()
    private val reminderManager : ReminderManager
        get() = ReminderManager(context)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = FragmentReminderManagementBinding.inflate(inflater)
        binding.handler = this

        val adapter = ReminderArrayAdapter(activity, reminderString, this)
        binding.reminderList.adapter = adapter

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reminder_text.text = getString(R.string.reminder_text, Calendar.getInstance().get(Calendar.YEAR))

        updateReminderList()
    }

    override fun onDeleteReminder(position: Int) {
        reminderString.removeAt(position)
        reminderManager.removeReminderFromCalendar(position)
        reminderManager.removeReminder(position)
        (reminder_list.adapter as ReminderArrayAdapter).notifyDataSetChanged()
    }

    override fun onUpdateReminder(position: Int) {
        val reminders = reminderManager.load().reminders
        val reminder = reminders.elementAt(position)

        val year = reminder.date.get(Calendar.YEAR)
        val month = reminder.date.get(Calendar.MONTH)
        val day = reminder.date.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(activity, {view, year, month, dayOfMonth ->  updateReminder(position, reminder, year, month, dayOfMonth)}, year, month, day).show()
    }

    private fun updateReminder(position: Int, reminder: Reminder, newYear: Int, newMonth: Int, newDay: Int){
        val calender = GregorianCalendar(newYear, newMonth, newDay)

        if(checkDate(calender)){
            val dialog  = InvalidDateDialog(context)
            dialog.show()
            return
        }

        reminderManager.update(position, reminder, calender)

        reminderString.clear()
        reminderManager.updateReminderToCalendar(reminder.id, calender)

        updateReminderList()
        (reminder_list.adapter as ReminderArrayAdapter).notifyDataSetChanged()
    }

    override fun onAddReminder() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(context, {view, year, month, dayOfMonth ->  addReminder(year, month, dayOfMonth)}, year, month, day).show()
    }

    private fun addReminder(year: Int, month: Int, day: Int) {
        val calendar = GregorianCalendar(year, month, day)
        if(checkDate(calendar)){
            val dialog  = InvalidDateDialog(context)
            dialog.show()
            return
        }

        val addCalendarDialog = AddToCalendarDialog(context, calendar, DialogInterface.OnClickListener({ dialog, wich ->
            val eventId = reminderManager.addReminderToCalendar(calendar,
                    getString(R.string.reminder_management_fragment_calender_title),
                    getString(R.string.reminder_management_fragment_calender_description))
            saveReminder(calendar, eventId)
        }), DialogInterface.OnClickListener({ dialog, wich -> saveReminder(calendar, -1) }))
        addCalendarDialog.show();
    }

    private fun checkDate(calendar : Calendar) : Boolean {
        val yesterday = Calendar.getInstance()
        yesterday.add(Calendar.DATE, -1);
        return calendar.before(yesterday)
    }

    private fun saveReminder(calendar : Calendar, eventId: Long){
        val reminder = Reminder(eventId, calendar)
        reminderManager.save(reminder)

        NotificationScheduler.setReminder(context, calendar)
        reminderString.add(createReminderString(calendar))
        (reminder_list.adapter as ReminderArrayAdapter).notifyDataSetChanged()
    }

    private fun updateReminderList(){
        val reminders = reminderManager.load()
        reminders.reminders.forEach { reminder -> reminderString.add(createReminderString(reminder.date))}
    }

    private fun createReminderString(calendar : Calendar) : String {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)

        return getString(R.string.reminder_management_fragment_string,
                dateFormat.format(calendar.time) ,
                reminderManager.getTimeDifference(calendar))
    }

    companion object {

        fun newInstance(): ReminderManagementFragment {
            return ReminderManagementFragment()
        }
    }

}
interface ReminderManagementFragmentHandler {
    fun onAddReminder()
}
