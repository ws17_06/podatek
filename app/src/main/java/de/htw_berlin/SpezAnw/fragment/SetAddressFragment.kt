package de.htw_berlin.SpezAnw.fragment

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import de.htw_berlin.SpezAnw.databinding.FragmentSetAddressBinding
import de.htw_berlin.SpezAnw.dialog.FinanceOfficeFoundDialog
import de.htw_berlin.SpezAnw.dialog.NoInternetDialog
import de.htw_berlin.SpezAnw.dialog.NoPlzDialog
import de.htw_berlin.SpezAnw.manager.FinanceManager
import kotlinx.android.synthetic.main.fragment_set_address.*


class SetAddressFragment : Fragment(), SetAddressFragmentHandler {

    private val financeManager = FinanceManager()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding = FragmentSetAddressBinding.inflate(layoutInflater)
        binding.handler = this

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        plz.requestFocus()
        showKeyboard()

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onSearch() {
        financeManager.getFinanceObservable().subscribe({ result ->

            val filtered = result.index.filter {
                financeOffice ->
                financeOffice.bereich.split(",").any { data -> data == plz.text.toString()
                }
            }

            if(filtered.isNotEmpty()) {
                // found finance office, ask user to save
                financeManager.save(filtered[0], context)

                val financeOfficeFoundDialog = FinanceOfficeFoundDialog(context, filtered[0].name,
                        DialogInterface.OnClickListener({ _, _ ->  closeKeyboard(); fragmentManager.popBackStackImmediate() })
                        )
                financeOfficeFoundDialog.show()

            }else{
                // plz not found in the list of finance offices
                val noPlzDialog= NoPlzDialog(context, plz.text.toString())
                noPlzDialog.show()
            }

        }, { error ->
            // no Internet connection
            val noInternetDialog = NoInternetDialog(context)
            noInternetDialog.show()
        })
    }

    private fun closeKeyboard(){
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }
    private fun showKeyboard(){
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
    }

    companion object {

        fun newInstance(): SetAddressFragment {
            return SetAddressFragment()
        }
    }
}

interface SetAddressFragmentHandler {
    fun onSearch()
}
