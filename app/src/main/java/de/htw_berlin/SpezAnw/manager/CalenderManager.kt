package de.htw_berlin.SpezAnw.manager

import android.Manifest
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.CalendarContract
import java.util.*

class CalenderManager(val context: Context){

    fun insert(date : Calendar, title: String, description: String) : Long {
        if (PermissionManager.checkPermission(context, Manifest.permission.WRITE_CALENDAR)) {
            return -1
        }

        val contentResolver = context.contentResolver

        val calendarId = getCalenderId()
        val values = createReminderContent(calendarId, title, description, date.timeInMillis)
        val uri = contentResolver.insert(CalendarContract.Events.CONTENT_URI, values)

        return uri.lastPathSegment.toLong()
    }

    private fun getCalenderId() : Int{
        val contentResolver = context.contentResolver
        val cursor = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), arrayOf("_id"), null, null, null)

        if(cursor.count > 0) {
            cursor.moveToLast()
            val calendarId = cursor.getInt(0)

            cursor.close()

            return calendarId
        }
        return  -1
    }

    fun delete(eventId: Long){
        if(eventId == -1L || PermissionManager.checkPermission(context, Manifest.permission.WRITE_CALENDAR)){
            return
        }
        val contentResolver = context.contentResolver
        val eventUri = Uri.parse("content://com.android.calendar/events")
        val deleteUri = ContentUris.withAppendedId(eventUri, eventId)
        contentResolver.delete(deleteUri, null, null)
    }

    fun update(eventId: Long, date: Calendar){
        if (PermissionManager.checkPermission(context, Manifest.permission.WRITE_CALENDAR)) {
            return
        }

        if(eventId <= 0){
            return
        }

        val contentResolver = context.contentResolver
        val eventUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventId)

        val event = ContentValues()
        event.put(CalendarContract.Events.DTSTART, date.timeInMillis)
        event.put(CalendarContract.Events.DTEND, date.timeInMillis)

        contentResolver.update(eventUri, event, null, null)
    }

    private fun createReminderContent(calendarId: Int, title : String, description: String, date : Long ) : ContentValues {
        val values = ContentValues()
        values.put(CalendarContract.Events.DTSTART, date)
        values.put(CalendarContract.Events.ALL_DAY, true)
        values.put(CalendarContract.Events.DTEND, date)
        values.put(CalendarContract.Events.TITLE, title)
        values.put(CalendarContract.Events.DESCRIPTION, description)
        values.put(CalendarContract.Events.CALENDAR_ID, calendarId)
        values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance()
                .timeZone.id)

        return values
    }

}
