package de.htw_berlin.SpezAnw.manager

import android.content.Context
import com.google.gson.GsonBuilder
import de.htw_berlin.SpezAnw.model.FinanceOffice
import de.htw_berlin.SpezAnw.model.FinanceOffices
import de.htw_berlin.SpezAnw.reset.FinanceOfficeClient
import de.htw_berlin.SpezAnw.reset.OpenDataAPI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class FinanceManager {

    private val gson = GsonBuilder().create()
    private val client = FinanceOfficeClient(OpenDataAPI.create())

    fun getFinanceObservable(): Observable<FinanceOffices>{
        return client.getFinanceOffice()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
    }

    fun save(financeOffice: FinanceOffice, context: Context): Boolean{
        val sharedPreferences =  context.getSharedPreferences(this.javaClass.simpleName, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val financeOfficeJson = gson.toJson(financeOffice)

        return editor.putString(FAVORITE_FINANCE_OFFICE_KEY, financeOfficeJson).commit()
    }

    fun load(context: Context): FinanceOffice? {
        val sharedPreferences =  context.getSharedPreferences(this.javaClass.simpleName, Context.MODE_PRIVATE)
        val financeOfficeJson = sharedPreferences.getString(FAVORITE_FINANCE_OFFICE_KEY, "")

        return if(financeOfficeJson ==""){
            null
        }else {
            gson.fromJson(financeOfficeJson, FinanceOffice::class.java)
        }
    }

    companion object {
        const val FAVORITE_FINANCE_OFFICE_KEY = "favoriteFinanceOfficeKey"
    }
}