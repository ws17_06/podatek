package de.htw_berlin.SpezAnw.manager

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

object PermissionManager{

    fun requestPermission(activity: Activity){
        ActivityCompat.requestPermissions(activity,
                arrayOf(Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR),
                101)

    }

    fun checkPermission(context: Context, permission: String) : Boolean{
        return ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED
    }

}