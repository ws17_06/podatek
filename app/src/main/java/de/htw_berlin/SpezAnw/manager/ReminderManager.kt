package de.htw_berlin.SpezAnw.manager

import java.util.concurrent.TimeUnit
import android.content.Context
import android.database.sqlite.SQLiteException
import com.google.gson.GsonBuilder
import de.htw_berlin.SpezAnw.dialog.ErrorCalendarDialog
import de.htw_berlin.SpezAnw.model.Reminder
import de.htw_berlin.SpezAnw.model.Reminders
import java.util.*
import kotlin.collections.ArrayList

class ReminderManager(val context: Context) {

    private val gson = GsonBuilder().create()
    private val calenderManager = CalenderManager(context)

    fun getTimeDifference(calendar : Calendar) : Long {
        val now = Date().time
        val end = calendar.time.time
        return TimeUnit.MILLISECONDS.toDays(Math.abs((end - now)))
    }

    fun addReminderToCalendar(date : Calendar, title: String, description: String) : Long {
        date.set(Calendar.HOUR_OF_DAY, 1)
        date.set(Calendar.MINUTE, 1)

        return calenderManager.insert(date, title, description)
    }

    fun removeReminder(position: Int){
        val reminders = load()
        val reminder = reminders.reminders.elementAt(position)
        reminders.reminders.remove(reminder)

        save(reminders)
    }

    fun removeReminderFromCalendar(position: Int){
        val reminders = load()
        val reminder = reminders.reminders.elementAt(position)
        calenderManager.delete(reminder.id)
    }

    fun updateReminderToCalendar(eventID: Long, date : Calendar){
        date.set(Calendar.HOUR_OF_DAY, 1)
        date.set(Calendar.MINUTE, 1)

        try {
            calenderManager.update(eventID, date)
        }catch (e: SQLiteException) {
            val dialog = ErrorCalendarDialog(context)
            dialog.show()
        }
    }

    fun save(reminder: Reminder): Boolean{
        val reminders = load()
        reminders.reminders.add(reminder)

        return save(reminders)
    }

    fun update(position: Int, reminder: Reminder, newDate: Calendar){
        reminder.date = newDate

        val reminders = load()
        reminders.reminders.removeAt(position)
        reminders.reminders.add(reminder)

        save(reminders)
    }

    private fun save(reminders: Reminders) : Boolean{
        val sharedPreferences =  context.getSharedPreferences(this.javaClass.simpleName, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        val reminderJson = gson.toJson(reminders)

        return editor.putString(REMINDERS_KEY, reminderJson).commit()
    }

    fun load(): Reminders {
        val sharedPreferences =  context.getSharedPreferences(this.javaClass.simpleName, Context.MODE_PRIVATE)
        val remindersJson = sharedPreferences.getString(REMINDERS_KEY, "")

        return if(remindersJson == ""){
            Reminders(ArrayList())
        }else {
            gson.fromJson(remindersJson, Reminders::class.java)
        }
    }

    companion object {
        const val REMINDERS_KEY = "remindersKey"
    }
}