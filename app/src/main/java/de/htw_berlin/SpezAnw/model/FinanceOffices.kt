package de.htw_berlin.SpezAnw.model

class FinanceOffices(val index: Array<FinanceOffice>)

class FinanceOffice(
        val id: String = "",
        val name: String = "",
        val oeffnungszeiten : String = "",
        val strasse : String = "",
        val telefon : String = "",
        val bereich : String = "",
        val email : String = "")