package de.htw_berlin.SpezAnw.model

import java.util.*
import kotlin.collections.ArrayList

class Reminders(val reminders: ArrayList<Reminder>)

class Reminder(
        val id: Long,
        var date : Calendar)