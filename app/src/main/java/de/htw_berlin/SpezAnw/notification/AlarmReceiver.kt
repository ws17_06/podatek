package de.htw_berlin.SpezAnw.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.htw_berlin.SpezAnw.MainActivity
import de.htw_berlin.SpezAnw.R

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        NotificationScheduler.showNotification(context, MainActivity::class.java,
                context.getString(R.string.notification_title), context.getString(R.string.notification_content))

    }
}


