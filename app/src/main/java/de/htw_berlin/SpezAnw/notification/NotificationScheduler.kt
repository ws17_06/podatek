package de.htw_berlin.SpezAnw.notification

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import de.htw_berlin.SpezAnw.R
import java.util.*


object NotificationScheduler {

    private val PRIMARY_CHANNEL = "default"
    private val CHANNEL_NAME = "name"
    private val DAILY_REMINDER_REQUEST_CODE = 100
    private val cls = AlarmReceiver::class.java
    private val defaultHour = 18;
    private val defaultMin = 0;

    fun setReminder(context: Context, calendar: Calendar) {
        val alarmManager = context.getSystemService(ALARM_SERVICE) as AlarmManager

        val receiver = ComponentName(context, cls)
        val pm = context.packageManager

        calendar.set(Calendar.HOUR_OF_DAY, defaultHour)
        calendar.set(Calendar.MINUTE, defaultMin)

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)

        val intent = Intent(context, cls)
        val pendingIntent = PendingIntent.getBroadcast(context, DAILY_REMINDER_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)
    }

    fun showNotification(context: Context, cls: Class<*>, title: String, content: String) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val pendingIntent = createIntent(context, cls)
        val notification = createNotification(context, pendingIntent, title, content)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(createNotificationChannel())
        }

        notificationManager.notify(DAILY_REMINDER_REQUEST_CODE, notification)
    }

    private fun createIntent(context: Context, cls: Class<*>) : PendingIntent {
        val notificationIntent = Intent(context, cls)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(cls)
        stackBuilder.addNextIntent(notificationIntent)

        return stackBuilder.getPendingIntent(DAILY_REMINDER_REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    private fun createNotification(context: Context, intent : PendingIntent, title: String, content: String) :Notification {
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, PRIMARY_CHANNEL)

        return builder.setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSound(alarmSound)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(intent).build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): NotificationChannel{
        val channel = NotificationChannel(PRIMARY_CHANNEL,
                CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)

        return channel
    }
}
