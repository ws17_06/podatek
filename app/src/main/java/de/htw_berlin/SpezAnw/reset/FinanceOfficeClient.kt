package de.htw_berlin.SpezAnw.reset

import io.reactivex.Observable
import de.htw_berlin.SpezAnw.model.FinanceOffices

class FinanceOfficeClient(private val api: OpenDataAPI) {
    fun getFinanceOffice(): Observable<FinanceOffices> {
        return api.getFinanceOffice()
    }
}