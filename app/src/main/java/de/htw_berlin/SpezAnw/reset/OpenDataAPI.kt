package de.htw_berlin.SpezAnw.reset

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import de.htw_berlin.SpezAnw.model.FinanceOffices

interface OpenDataAPI {

    @GET("index.php/index.json?page=1")
    fun getFinanceOffice()
            : Observable<FinanceOffices>

    /**
     * Companion object to create the GithubApiService
     */
    companion object Factory {
        fun create(): OpenDataAPI {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://www.berlin.de/sen/finanzen/steuern/finanzaemter/zustaendigkeit-finden/")
                    .build()

            return retrofit.create(OpenDataAPI::class.java)
        }
    }
}